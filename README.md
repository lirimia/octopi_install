# Install OS
- burn RaspiOS Buster image on a SD card (2021-03-04-raspios-buster-armhf-lite)
- add an empty ssh file on boot partition
- add a wpa_supplicant.conf containing the wireless SSID and password

# Configure Raspberry PI
- run 'sudo raspi-config' to:
    - configure hostname (OctoPI) (Network Options/Hostname)
    - configure timezone (Localisation Options/Timezone)
    - enable webcam (Interface Options/Camera)
- curl -L https://bitbucket.org/lirimia/octopi_install/raw/master/setup.sh | bash | tee -a /home/pi/install.log

# OctoPI Printer configuration
- get the latest backup from Dropbox
- pick Restore Backup option from Setup Wizard

# OctoPI Slicer configuration (Docker on Mac)
- get Cura profiles from BitBucket
- Setup Wizard
    - Disable Access Control
    - Disable Anonymous Usage Tracking
    - Enable Connectivity Check
    - Enable Plugin Blacklist Processing
    - CuraEngine
        - Path to CuraEngine: /opt/cura/CuraEngine; DO NOT TEST 
        - Import Profile ...
            - Identifier: 15.04.2
            - Name:       Cura
    - Default Printer Profile
        - General
            - Name: SmartCore
        - Print bed & build volume
            - Height (Z): 85
- Settings
    - Plugin Manager - disable the following plugins
        - Action Command Prompt Support
        - Announcement Plugin
        - Anonymous Usage Tracking
        - Application Keys Plugin
        - Discovery
        - Printer Safety Check
        - Software Update
        - Virtual Printer
    - Backup & Restore
        - restore from octoprint_slicer_backup.zip
- restart the container using 'Restart slicer' command
- Settings
    - Features
        - disable Temperature Graph
        - disable Confirm before cancelling
        - disable SD support
        - disable Keyboard Control
    - Appearance
        - Title: OctoPI Slicer
    - CuraEngine
        - test path to CuraEngine
        - enable 'Log the output of CuraEngine to plugin_cura_engine.log'
    - Tab Order
        - Assigned Tabs
            - plugin_slicer
            - plugin_stlviewer
        - Hidden Tabs (having 'Show Tab Name' option disabled)       
            - control
            - gcodeviewer
            - temperature
            - terminal
- on the left pane, drag 'Files' section on top of the other tabs
- restart the container again using 'Restart slicer' command