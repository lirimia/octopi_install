#!/bin/bash

SUDO=''
[[ $(id -u) -ne 0 ]] && SUDO='sudo'

[ $# -eq 0 ] && DEBUG=false
[ $# -eq 1 ] && [ $1 == '-debug' ] && DEBUG=true

[ $DEBUG = false ]

# wait a minute for everything to settle down
sleep 60

# sync backups to Dropbox first
rclone sync -v /home/pi/.octoprint/data/backup dropbox:$(hostname)

# subscribe to MQTT topic
mosquitto_sub -h 10.0.0.10 -t 'octoprint/power/#' -v | while read -r topic payload

do
    [ $DEBUG = true ] && echo "${topic}: ${payload}"

    if [ $(echo ${topic} | grep 'web' | wc -l) -eq 1 ]; then
         if [ ${payload} == 'OFF' ] || [ ${payload} == '0' ]; then
             /home/pi/scripts/mqttPub.sh
         fi
    fi
done
