#!/bin/bash

echo
echo "**************************************************"
echo "* Update linux                                   *"
echo "**************************************************"
sudo apt-get update
sudo apt-get upgrade -y


echo
echo "**************************************************"
echo "* Install required packages                      *"
echo "**************************************************"
sudo apt-get install -y dos2unix
sudo apt-get install -y mc
sudo apt-get install -y mosquitto-clients


echo
echo "**************************************************"
echo "* Install and configure git                      *"
echo "**************************************************"
sudo apt-get install -y git

git config --global user.name 'Laur'
git config --global user.email 'lirimia@gmail.com'


echo
echo "**************************************************"
echo "* Install OctoPrint in the virtual environment   *"
echo "**************************************************"
sudo apt-get install -y python3-pip
sudo apt-get install -y python3-dev
sudo apt-get install -y python3-setuptools
sudo apt-get install -y python3-venv
sudo apt-get install -y libyaml-dev build-essential

python3 -m venv oprint
source oprint/bin/activate

pip install pip --upgrade
pip install --no-cache-dir octoprint

pip3 install https://github.com/p3p/pyheatshrink/releases/download/0.3.3/pyheatshrink-pip.zip
pip3 install marlin-binary-protocol

deactivate

wget https://github.com/OctoPrint/OctoPrint/raw/master/scripts/octoprint.service
sed -i 's|ExecStart=/home/pi/OctoPrint/venv/bin/octoprint|ExecStart=/home/pi/oprint/bin/octoprint|' octoprint.service

sudo mv octoprint.service /etc/systemd/system/octoprint.service
sudo systemctl enable octoprint.service
sudo systemctl start octoprint.service


echo
echo "**************************************************"
echo "* Install and configure HAProxy                  *"
echo "**************************************************"
sudo apt-get install -y haproxy

curl -L https://bitbucket.org/lirimia/octopi_install/raw/master/haproxy.cfg -o /home/pi/haproxy.cfg

sudo mv /home/pi/haproxy.cfg /etc/haproxy/haproxy.cfg
sudo chown root:root /etc/haproxy/haproxy.cfg


echo
echo "**************************************************"
echo "* Enable webcam streaming/timelapse              *"
echo "**************************************************"
sudo apt-get install -y subversion libjpeg62-turbo-dev imagemagick ffmpeg libv4l-dev cmake

git clone https://github.com/jacksonliam/mjpg-streamer.git
cd mjpg-streamer/mjpg-streamer-experimental
export LD_LIBRARY_PATH=.
make
cd ~

sudo sed -i "s|^\exit 0||" /etc/rc.local
echo "# Start webcam on boot" | sudo tee -a /etc/rc.local
echo "su - pi -c '/home/pi/scripts/webcam start'" | sudo tee -a /etc/rc.local
echo "" | sudo tee -a /etc/rc.local
echo "exit 0" | sudo tee -a /etc/rc.local


echo
echo "**************************************************"
echo "* Get scripts                                    *"
echo "**************************************************"
mkdir /home/pi/scripts

curl -L https://bitbucket.org/lirimia/octopi_install/raw/master/scripts/mqttPub.sh -o /home/pi/scripts/mqttPub.sh
curl -L https://bitbucket.org/lirimia/octopi_install/raw/master/scripts/mqttSub.sh -o /home/pi/scripts/mqttSub.sh

curl -L https://bitbucket.org/lirimia/octopi_install/raw/master/webcam.sh -o /home/pi/scripts/webcam
curl -L https://bitbucket.org/lirimia/octopi_install/raw/master/webcamDaemon.sh -o /home/pi/scripts/webcamDaemon

chmod +x /home/pi/scripts/*

echo '@reboot /home/pi/scripts/mqttSub.sh &' | sudo tee -a /var/spool/cron/pi


echo
echo "**************************************************"
echo "* Static IP                                      *"
echo "**************************************************"
echo "" | sudo tee -a /etc/dhcpcd.conf
echo "# Static IP Configuration" | sudo tee -a /etc/dhcpcd.conf
echo "interface wlan0" | sudo tee -a /etc/dhcpcd.conf
echo "static ip_address=10.0.0.11/24" | sudo tee -a /etc/dhcpcd.conf
echo "static routers=10.0.0.1" | sudo tee -a /etc/dhcpcd.conf
echo "static domain_name_servers=10.0.0.10" | sudo tee -a /etc/dhcpcd.conf


echo
echo "**************************************************"
echo "* Change passowrd for pi user account            *"
echo "**************************************************"
echo -e "raspberry\noctoprint\noctoprint" | passwd

sudo usermod -a -G tty pi
sudo usermod -a -G dialout pi
sudo usermod -a -G video pi
echo


echo
echo "**************************************************"
echo "* Cleaning linux                                 *"
echo "**************************************************"
/usr/bin/crontab /var/spool/cron/pi

sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get autoremove -y
sudo apt-get autoclean -y


echo
echo "**************************************************"
echo "* Append script VersionID to log file            *"
echo "**************************************************"
git clone  --quiet https://bitbucket.org/lirimia/octopi_install.git
cd octopi_install/
version=$(git log --oneline | head -1 | awk '{print $1}')
cd ..
rm -rf octopi_install

echo "Logfile for OctoPI Install script version $version"
echo "Installation completed!"

sudo reboot