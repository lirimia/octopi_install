#!/bin/bash

mkdir -p /Users/lairimia/Docker/octoslice
docker run \
    --rm \
	--name slicer \
	-dit \
	-v /Users/lairimia/Docker/octoslice:/home/octoprint/.octoprint \
	--device /dev/null:/dev/ttyACM0 \
	-p 5000:5000 \
	acrelle/rpi-octoprint:latest
	
sleep 60
docker stop slicer
curl -L https://bitbucket.org/lirimia/octopi_install/raw/master/octoprint_slicer_backup.zip -o /Users/lairimia/Docker/octoslice/data/backup/octoprint_slicer_backup.zip

docker run \
	--name slicer \
	-dit \
	-v /Users/lairimia/Docker/octoslice:/home/octoprint/.octoprint \
	--device /dev/null:/dev/ttyACM0 \
	-p 5000:5000 \
	acrelle/rpi-octoprint:latest
